FROM ubuntu:14.04
MAINTAINER Nathan Valentine - nrvale0@gmail.com

ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_NONINTERACTIVE_SEEN true

ENV SSH_USERNAME admin
ENV SSH_PASSWORD ubnt
ENV MFI_ADDR power
ENV MFI_RELAY relay1
ENV POLL_URL http://www.google.com
ENV POLL_INTERVAL 60

RUN apt-get update
RUN apt-get install -y python-paramiko wget

RUN mkdir -p /app/bin
ADD src/bin/mpowertwiddle /app/bin/mpowertwiddle
ENTRYPOINT /app/bin/mpowertwiddle
