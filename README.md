# nrvale0/mpowertwiddle

## Summary
Poll a URI until failure. Upon failure, login to Ubiquiti mFI mPower device and reset configured relay.
Use case: If WAN connection is down power cycle the device providing WAN connectivity.


## Repository
https://github.com/nrvale0/docker-mpowertwiddle


## Author
Nathan Valentine
* [nrvale0@gmail.com](mailto:nrvale0@gmail.com)
* https://about.me/nrvale0
